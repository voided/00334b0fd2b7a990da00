{Robot, Adapter, TextMessage, EnterMessage, LeaveMessage, Response} = require 'hubot'

SkypeHostServer = require './skypehost'


class SkypeHost extends Adapter

  send: (envelope, strings...) ->
    @server.sendChat str for str in strings

  emote: (envelope, strings...) ->
    @server.sendEmote str for str in strings

  reply: (envelope, strings...) ->
    @send envelope, "#{envelope.user.name}: #{str}" for str in strings


  run: ->
    @server = new SkypeHostServer 8880

    @server.on 'chat', (user, message) =>
      console.log user, ':', message
      @receive new TextMessage( user, message )
      
    @server.start()

    # tell hubot we're ready
    @emit 'connected'

exports.use = (robot) ->
  new SkypeHost robot
